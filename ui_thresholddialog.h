/********************************************************************************
** Form generated from reading UI file 'thresholddialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_THRESHOLDDIALOG_H
#define UI_THRESHOLDDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ThresholdDialog
{
public:
    QPushButton *thresholdOKPushButton;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QSpinBox *spinBoxArea;
    QLabel *label;
    QSpinBox *spinBoxIntensity;
    QLabel *label_2;

    void setupUi(QDialog *ThresholdDialog)
    {
        if (ThresholdDialog->objectName().isEmpty())
            ThresholdDialog->setObjectName(QStringLiteral("ThresholdDialog"));
        ThresholdDialog->resize(163, 121);
        QIcon icon;
        icon.addFile(QStringLiteral(":/emersonLogo.JPG"), QSize(), QIcon::Normal, QIcon::Off);
        ThresholdDialog->setWindowIcon(icon);
        thresholdOKPushButton = new QPushButton(ThresholdDialog);
        thresholdOKPushButton->setObjectName(QStringLiteral("thresholdOKPushButton"));
        thresholdOKPushButton->setGeometry(QRect(40, 80, 75, 23));
        layoutWidget = new QWidget(ThresholdDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 20, 117, 48));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        spinBoxArea = new QSpinBox(layoutWidget);
        spinBoxArea->setObjectName(QStringLiteral("spinBoxArea"));
        spinBoxArea->setMinimum(25);
        spinBoxArea->setMaximum(200);
        spinBoxArea->setSingleStep(25);

        gridLayout->addWidget(spinBoxArea, 0, 1, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        spinBoxIntensity = new QSpinBox(layoutWidget);
        spinBoxIntensity->setObjectName(QStringLiteral("spinBoxIntensity"));
        spinBoxIntensity->setMinimum(15);
        spinBoxIntensity->setMaximum(80);
        spinBoxIntensity->setSingleStep(5);

        gridLayout->addWidget(spinBoxIntensity, 1, 1, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);


        retranslateUi(ThresholdDialog);

        QMetaObject::connectSlotsByName(ThresholdDialog);
    } // setupUi

    void retranslateUi(QDialog *ThresholdDialog)
    {
        ThresholdDialog->setWindowTitle(QApplication::translate("ThresholdDialog", "Set Thresholds", 0));
        thresholdOKPushButton->setText(QApplication::translate("ThresholdDialog", "OK", 0));
        label->setText(QApplication::translate("ThresholdDialog", "Area", 0));
        label_2->setText(QApplication::translate("ThresholdDialog", "Intesnsity", 0));
    } // retranslateUi

};

namespace Ui {
    class ThresholdDialog: public Ui_ThresholdDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_THRESHOLDDIALOG_H
