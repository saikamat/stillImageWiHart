/*******************************************************************************
* Filename              :timestamp.cpp
* Description of file	:This source file monitors for existence of timestamps
*                        and emits a signal whenever a new timestamp file is
*                        received.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "timestampthread.h"
#include <iostream>
/*******************************************************************************
* Function      :TimeStampThread
* Description   :This is a simple constructor.
* Assumptions	:NA
* Parameters	:QObject *parent
* Returns       :NA
*******************************************************************************/
TimeStampThread::TimeStampThread(QObject *parent) :
    QThread(parent)
{

}

/*******************************************************************************
* Function      :run
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void TimeStampThread::run()
{
    exec();
}

/*******************************************************************************
* Function      :exec
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :An integer
*                0-> on success
*******************************************************************************/
tS32 TimeStampThread::exec()
{
    while(1)
    {
        monitorForTimeStamps();
        emit(testSignal("hello world again!!"));  // emits a test signal
        sleep(SLEEP_VALUE_2);
    }
    return 0;
}

/*******************************************************************************
* Function      :monitorForTimeStamps
* Description   :This function is executed when a thread for checking timestamps
*                is started. It checks whether text file containing the timestamp
*                exists at a pre-designated path, compares its contents and emits
*                a signal.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void TimeStampThread::monitorForTimeStamps()
{
    QFile::remove(TIMESTAMP_MESSAGE_STORAGE_PATH);
    QFile timeStampFile(TIMESTAMP_MESSAGE_STORAGE_PATH);
    timeStampFile.open(QIODevice::WriteOnly);
    timeStampFile.close();
    QFile inputFile(TIMESTAMP_MESSAGE_STORAGE_PATH);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QFile::remove(TIMESTAMP_MESSAGE_STORAGE_PATH);
        while(inputFile.size() == EMPTY_FILE_SIZE_VALUE)
        {
           cout<<"monitorForTimeStamps" <<endl;
           // Sleep(RESPONSE_SLEEP_VALUE);
        }
        QTextStream in(&inputFile);
        tS32 tab[9];tS32 i = 0;
        while ( !in.atEnd() )
        {
            QString strLine = in.readLine();
            tab[i] = strLine.toInt();
            i++;
            if(1 == i)
            {
                if(((strLine.compare(strLine, REFERENCE_TIMESTAMP_ID,
                                     Qt::CaseSensitive)) == 0) |
                        ((strLine.compare(strLine, DELTA_TIMESTAMP_ID,
                                          Qt::CaseSensitive)) == 0) |
                        ((strLine.compare(strLine, CURRENT_TIMESTAMP_ID,
                                          Qt::CaseSensitive)) == 0))
                {
                     emit(timeStampSignal(strLine));     // connects to timeStampSlot on
                                                        // signal emission
                  }
            }
        }
        inputFile.close();
    }
}
