/*******************************************************************************
* Filename              :alertsthread.cpp
* Description of file	:This source file monitors for existence of alerts and
*                        emits a signal whenever an alert occurs.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Novenber 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014, ,
*                        All Rights Reserved.
*******************************************************************************/
#include "alertsthread.h"
#include <fstream>

/*******************************************************************************
* Function      :AlertsThread
* Description   :This is a simple constructor.
* Assumptions	:NA
* Parameters	:QObject *parent
* Returns       :NA
*******************************************************************************/
AlertsThread::AlertsThread(QObject *parent) :
    QThread(parent)
{

}

/*******************************************************************************
* Function      :run
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void AlertsThread::run()
{
    exec();
}

/*******************************************************************************
* Function      :exec
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :An integer
*                0-> on success
*******************************************************************************/
tS32 AlertsThread::exec()
{
    while(1)
    {
        monitorForAlerts();
        emit(testSignal("hello world!!")); // emits a test signal
        sleep(SLEEP_VALUE_2);
    }
    return 0;
}

/*******************************************************************************
* Function      :monitorForAlerts
* Description   :This function is executed when a thread is started. It checks
*                whether text file containing the alert message exists at a
*                pre-designated path, compares its contents and emits an alert
*                signal.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void AlertsThread::monitorForAlerts()
{
    QFile::remove(ALERTS_MESSAGE_STORAGE_PATH);
    QFile alertFile(ALERTS_MESSAGE_STORAGE_PATH);
    alertFile.open(QIODevice::WriteOnly);
    alertFile.close();
    QFile inputFile(ALERTS_MESSAGE_STORAGE_PATH);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QFile::remove(ALERTS_MESSAGE_STORAGE_PATH);
        while(inputFile.size() == EMPTY_FILE_SIZE_VALUE)
        {
            cout<<"monitorForAlerts" <<endl;
           // Sleep(RESPONSE_SLEEP_VALUE);
        }
        QTextStream in(&inputFile);
        tS32 tab[9]; tS32 i=0;

        QString strLine = in.readLine();
        tab[i]=strLine.toInt();
        i++;
        if(1 == i)
        {
            if(((strLine.compare(strLine, ALERT1, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT2, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT3, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT4, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT5, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT6, Qt::CaseSensitive)) == 0) |
               ((strLine.compare(strLine, ALERT7, Qt::CaseSensitive)) == 0) )
            {
                emit(alertSignal(strLine));   // connects to alertSlot on signal emission
            }
        }
        inputFile.close();
    }
}
