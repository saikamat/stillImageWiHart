/********************************************************************************
** Form generated from reading UI file 'intervaldialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTERVALDIALOG_H
#define UI_INTERVALDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IntervalDialog
{
public:
    QPushButton *intervalOKPushButton;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QSpinBox *spinBoxReference;
    QLabel *label_2;
    QLabel *label_3;
    QSpinBox *spinBoxDelta;
    QLabel *label;

    void setupUi(QDialog *IntervalDialog)
    {
        if (IntervalDialog->objectName().isEmpty())
            IntervalDialog->setObjectName(QStringLiteral("IntervalDialog"));
        IntervalDialog->resize(163, 121);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Logo.JPG"), QSize(), QIcon::Normal, QIcon::Off);
        IntervalDialog->setWindowIcon(icon);
        intervalOKPushButton = new QPushButton(IntervalDialog);
        intervalOKPushButton->setObjectName(QStringLiteral("intervalOKPushButton"));
        intervalOKPushButton->setGeometry(QRect(50, 90, 75, 23));
        layoutWidget = new QWidget(IntervalDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 10, 123, 67));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 1, 2, 1, 1);

        spinBoxReference = new QSpinBox(layoutWidget);
        spinBoxReference->setObjectName(QStringLiteral("spinBoxReference"));
        spinBoxReference->setMinimum(10);
        spinBoxReference->setMaximum(120);
        spinBoxReference->setSingleStep(10);

        gridLayout->addWidget(spinBoxReference, 1, 1, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 0, 2, 1, 1);

        spinBoxDelta = new QSpinBox(layoutWidget);
        spinBoxDelta->setObjectName(QStringLiteral("spinBoxDelta"));
        spinBoxDelta->setMinimum(5);
        spinBoxDelta->setMaximum(60);

        gridLayout->addWidget(spinBoxDelta, 0, 1, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);


        retranslateUi(IntervalDialog);

        QMetaObject::connectSlotsByName(IntervalDialog);
    } // setupUi

    void retranslateUi(QDialog *IntervalDialog)
    {
        IntervalDialog->setWindowTitle(QApplication::translate("IntervalDialog", "Set Durations", 0));
        intervalOKPushButton->setText(QApplication::translate("IntervalDialog", "OK", 0));
        label_4->setText(QApplication::translate("IntervalDialog", "min", 0));
        label_2->setText(QApplication::translate("IntervalDialog", "Reference", 0));
        label_3->setText(QApplication::translate("IntervalDialog", "min", 0));
        label->setText(QApplication::translate("IntervalDialog", "Delta", 0));
    } // retranslateUi

};

namespace Ui {
    class IntervalDialog: public Ui_IntervalDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTERVALDIALOG_H
