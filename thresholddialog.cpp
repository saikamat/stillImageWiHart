/*******************************************************************************
* Filename              :thresholddialog.cpp
* Description of file	:This source file accepts user strResponse to change the
*                        area and intesnity thresholds and writes the values
*                        to a text file.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "thresholddialog.h"
#include "ui_thresholddialog.h"

/*******************************************************************************
* Function      :ThresholdDialog
* Description   :This is a simple constructor, used to setup the UI window.
* Assumptions	:NA
* Parameters	:QWidget *parent
* Returns       :NA
*******************************************************************************/
ThresholdDialog::ThresholdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ThresholdDialog)
{
    ui->setupUi(this);
}

/*******************************************************************************
* Function      :~ThresholdDialog
* Description   :This is a simple destructor.
* Assumptions	:NA
* Parameters	:NA
* Returns       :NA
*******************************************************************************/
ThresholdDialog::~ThresholdDialog()
{
    delete ui;
}

/*******************************************************************************
* Function      :on_thresholdOKPushButton_clicked
* Description   :This function is executed when the user wishes to change the
*                area and intensity thresholds. The values are loaded to a text
*                file, which is subsequently accepted by device.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void ThresholdDialog::on_thresholdOKPushButton_clicked()
{
    QFile::remove(CMD_HANDLER_STORAGE_PATH);
    QString filename = CMD_HANDLER_STORAGE_PATH;
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << "updateThreshold 4 "<<ui->spinBoxArea->text()
               <<" "<<ui->spinBoxIntensity->text();
        file.close();
        QString strResponse = USB_RESPONSE_PATH;
        QFile resp(strResponse);
        resp.open(QIODevice::WriteOnly);
        resp.close();
        QFile resp1(strResponse);
        tU8 cBuf[CHARACTER_BUFFER_VALUE];
        tU8 cBuf1[] = USB_RESPONSE_OK;
        tU8 cBuf2[] = USB_RESPONSE_INVALID;

        resp1.open(QIODevice::ReadOnly);
        while(resp1.size() == EMPTY_FILE_SIZE_VALUE)
        {
            cout<<"on_thresholdOKPushButton_clicked" <<endl;
           // Sleep(RESPONSE_SLEEP_VALUE);
        }
        qint64 lineLength = resp1.readLine(cBuf, sizeof(cBuf));
        resp1.close();
        if(strcmp(cBuf, cBuf1) == 0)
        {
            QFile::remove(CMD_HANDLER_STORAGE_PATH);
            QFile::remove(USB_RESPONSE_PATH);
        }
        if(strcmp(cBuf, cBuf2) == 0)
        {
            QFile::remove(CMD_HANDLER_STORAGE_PATH);
            QFile::remove(USB_RESPONSE_PATH);
            QMessageBox::critical(this,
                                  tr("ERROR"),
                                  tr("Command not Recieved.\nPlease try sending "
                                     "it again.\n"));
        }
        file.close();
    }
    this->close();
}
