/*******************************************************************************
* Filename              :rtcrequestthread.cpp
* Description of file	:This source file monitors for existence of rtcRequest
*                        and emits a signal whenever a new rtcRequest file is
*                        received.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :November 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "rtcrequestthread.h"

/*******************************************************************************
* Function      :RTCRequestThread
* Description   :This is a simple constructor.
* Assumptions	:NA
* Parameters	:QObject *parent
* Returns       :NA
*******************************************************************************/
RTCRequestThread::RTCRequestThread(QObject *parent) :
    QThread(parent)
{

}

/*******************************************************************************
* Function      :run
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void RTCRequestThread::run()
{
    exec();
}

/*******************************************************************************
* Function      :exec
* Description   :This function is executed when a thread is started.
* Assumptions	:NA
* Parameters	:None
* Returns       :An integer
*                0-> on success
*******************************************************************************/
tS32 RTCRequestThread::exec()
{
    while(1)
    {
        monitorForRTCRequest();
        emit(testSignal("hello world!!")); // emits a test signal
        sleep(SLEEP_VALUE_2);
    }
    return 0;
}

/*******************************************************************************
* Function      :monitorForRTCRequest
* Description   :This function is executed when a thread is started. It checks
*                whether text file containing the rtc request mesaage exists at
*                a pre-designated path, compares its contents and emits a signal.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void RTCRequestThread::monitorForRTCRequest()
{
    QFile::remove(RTC_REQUEST_MESSAGE_STORAGE_PATH);
    QFile::remove(USB_RTC_RESPONSE_PATH);
    QFile rtcFile(RTC_REQUEST_MESSAGE_STORAGE_PATH);
    rtcFile.open(QIODevice::WriteOnly);
    rtcFile.close();

    QFile inputFile(RTC_REQUEST_MESSAGE_STORAGE_PATH);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QFile::remove(RTC_REQUEST_MESSAGE_STORAGE_PATH);
        while(inputFile.size() == EMPTY_FILE_SIZE_VALUE)
        {
            cout<<"monitorForRTCRequest" <<endl;
         //   Sleep(RESPONSE_SLEEP_VALUE);
        }
        emit(rtcRequestSignal());
        inputFile.close();
    }
}
