/*******************************************************************************
* Filename              :definitions.h
* Description of file	:This header file consists of all the include & macro
*                        definitions required.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Novenber 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef DEFINITIONS_H
#define DEFINITIONS_H
#pragma once

#define _CRT_SECURE_NO_DEPRECATE
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <QMainWindow>
#include <QScrollArea>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QDesktopServices>
#include <QUrl>
#include <QDateTime>
#include <QFileInfo>
#include <QTimer>
#include <QSignalMapper>
#include <QSpinBox>
#include <QLabel>
#include <QDebug>
#include <fstream>
#include <iostream>
#include <Windows.h>
#include <QThread>
#include <QtCore>
#include <QMessageBox>
#include <QDialog>
#include <math.h>
#include <opencv/cv.hpp>
#include <opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv/cv.h>
#include <opencv2/ml/ml.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2\features2d\features2d.hpp>
#include "alertsthread.h"
#include "timestampthread.h"
#include "rtcrequestthread.h"

#define SERVER_PATH                      "..\\..\\tftpServerStandAlone.exe"
#define CURRENT_IMAGE_STORAGE_PATH        "..//..//currentImg.jpg"
#define CURRENT_IMAGE_BACKUP_PATH        "..//..//backUpRepository//Current Images//currentImg[%1].JPG"
#define DELTA_IMAGE_STORAGE_PATH         "..//..//deltaImg.jpg"
#define DELTA_IMAGE_BACKUP_PATH          "..//..//backUpRepository//Delta Images//deltaImg[%1].JPG"
#define UPDATED_IMAGE_STORAGE_PATH       "..//..//updatedRefImg.jpg"
#define UPDATED_IMAGE_BACKUP_PATH        "..//..//backUpRepository//Reference Images//updatedRefImg[%1].JPG"
#define RECONSTRCUTED_IMAGE_STORAGE_PATH "..//..//Reconstructed.jpg"
#define RECONSTRCUTED_IMAGE_BACKUP_PATH  "..//..//backUpRepository//Reconstructed Images//Reconstructed[%1].JPG"
#define CMD_HANDLER_STORAGE_PATH         "..//..//commandHandler.txt"
#define USB_RESPONSE_PATH                "..//..//usbResponse.txt"
#define USB_RTC_RESPONSE_PATH            "..//..//usbRtcResponse.txt"
#define ALERTS_MESSAGE_STORAGE_PATH      "..//..//usbAlert.txt"
#define ALERT_MESSAGE_BACKUP_PATH        "..//..//alertsLogBackUp.txt"
#define ALERT_MESSAGE_LOG_PATH           "..//..//alertsLog.csv"
#define TIMESTAMP_MESSAGE_STORAGE_PATH   "..//..//timeStamp.txt"
#define RTC_REQUEST_MESSAGE_STORAGE_PATH "..//..//rtcRequest.txt"
#define CAR_CLASSIFIERS_PATH             "..//..//final_car.xml"
#define PEOPLE_CLASSIFIERS_PATH          "..//..//final_ppl.xml"
#define MANUAL_PATH                      "..\\..\\StillImageWiHartManual.doc"
#define DATE_TIME_FORMAT                 "MM-dd-yy hh_mm_ss"
#define DATE_TIME_FORMAT2                "MM-dd-yy hh:mm:ss"
#define USB_RESPONSE_OK                  "ok"
#define USB_RESPONSE_INVALID             "invalid"
#define ALERT1                           "alert1"
#define ALERT2                           "alert2"
#define ALERT3                           "alert3"
#define ALERT4                           "alert4"
#define ALERT5                           "alert5"
#define ALERT6                           "alert6"
#define ALERT7                           "alert7"
#define LED_ON_ID                        "on"
#define LED_OFF_ID                       "off"
#define REFERENCE_TIMESTAMP_ID           "Ref"
#define DELTA_TIMESTAMP_ID               "Delta"
#define CURRENT_TIMESTAMP_ID             "Current"
#define PERSON_DETECTED                  "Person detected"
#define VEHICLE_DETECTED                 "Vehicle detected"
#define TIMER_VALUE                    (tS32)           500
#define NOISE_THRESHOLD                (tS32)           20
#define WHITE                          (tS32)           255
#define ROTATION_ANGLE                 (tS32)           0
#define SLEEP_VALUE                    (tS32)           500
#define SLEEP_VALUE_2                  (tS32)           1
#define RESPONSE_SLEEP_VALUE           (tS32)           3000
#define TIMESTAMP_EXTRACTION_POINT     (tS32)           12
#define MAX_ROW_COUNT_ALERTS_LOG_TABLE (tS32)           25
#define REFERENCE_IMAGE_ID             (tS32)           0
#define DELTA_IMAGE_ID                 (tS32)           1
#define CURRENT_IMAGE_ID               (tS32)           2
#define INTERVAL_COMPARE_CONSTANT      (tS32)           0
#define EMPTY_FILE_SIZE_VALUE          (tS32)           0
#define CHARACTER_BUFFER_VALUE         (tS32)           121
#define COMMAND_HANDLE_BUFFER_VALUE    (tS32)           512
#define IMAGE_TIMESTAMP_EXTRACTION_ID  (tS32)           0
#define ALERT_TIMESTAMP_EXTRACTION_ID  (tS32)           1
#define TIMESTAMP_LINE_LOCATION_VALUE  (tS32)           2
#define LIGHTING_COND_LOCATION_VALUE   (tS32)           3
#define NO_OF_CHANNELS                 (tS32)           3
#define HOG_IMAGE_WIDTH                (tS32)           64
#define HOG_IMAGE_HEIGHT               (tS32)           128
#define VGA_WIDTH                      (tS32)           640
#define VGA_HEIGHT                     (tS32)           480
#define HVGA_WIDTH                     (tS32)           480
#define HVGA_HEIGHT                    (tS32)           320
#define QVGA_WIDTH                     (tS32)           320
#define QVGA_HEIGHT                    (tS32)           240
#define ROTATION_COEFFICIENT           (tS32)           0
#define CONTOUR_COEFFICIENT_1          (tS32)           2000
#define CONTOUR_COEFFICIENT_2          (tS32)           5000
#define PREDICT_VALUE                  (tS32)           1
#define CONTOUR_LINE_THICKNESS         (tS32)           40
#define CONTOUR_MAX_LEVEL              (tS32)           0
#define THRESHOLD_PARAMETER_VALUE      (tS32)           5
#define WHITE_PIXEL_VALUE              (tS32)           255
#define VEHICLE_DETECTION_INDEX        (tS32)           0
#define PERSON_DETECTION_INDEX         (tS32)           1


typedef float tF32;
typedef int tS32;
typedef char tU8;
typedef bool tBoolean;


using namespace cv;
using namespace std;

#endif // DEFINITIONS_H
