/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCaptureReferenceImage;
    QAction *actionCaptureDeltaImage;
    QAction *actionInterval;
    QAction *actionThreshold;
    QAction *actionQVGA;
    QAction *actionHVGA;
    QAction *actionVGA;
    QAction *actionViewReconstructedImage;
    QAction *actionViewRefAndDeltaImage;
    QAction *actionViewAllImages;
    QAction *actionSaveReconstructedImage;
    QAction *actionSaveReferenceImage;
    QAction *actionSaveDeltaImage;
    QAction *actionExport;
    QAction *actionClear;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QLabel *deltaImageLabel;
    QLabel *reconstructedImageTitle;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QLabel *referenceImageLabel;
    QLabel *deltaImageTitle;
    QLabel *reconstructedImageLabel;
    QSpacerItem *horizontalSpacer_5;
    QLabel *referenceImageTitle;
    QTableWidget *tableWidget;
    QMenuBar *menuBar;
    QMenu *menuCapture;
    QMenu *menuConfigure;
    QMenu *menuView;
    QMenu *menuResolution;
    QMenu *menuImage;
    QMenu *menuAlerts;
    QMenu *menuSave;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(904, 672);
        QIcon icon;
        icon.addFile(QStringLiteral(":/Logo.JPG"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QLatin1String("QMenuBar {background-color: rgb(0, 0, 0) }\n"
"QMenuBar::item { background-color: rgb(0, 0, 0); color: white }\n"
"QMenuBar::item:pressed{\n"
"	background-color: rgb(153, 153, 153);\n"
"	border: 1px solid white;\n"
"}"));
        actionCaptureReferenceImage = new QAction(MainWindow);
        actionCaptureReferenceImage->setObjectName(QStringLiteral("actionCaptureReferenceImage"));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        actionCaptureReferenceImage->setFont(font);
        actionCaptureDeltaImage = new QAction(MainWindow);
        actionCaptureDeltaImage->setObjectName(QStringLiteral("actionCaptureDeltaImage"));
        actionCaptureDeltaImage->setFont(font);
        actionInterval = new QAction(MainWindow);
        actionInterval->setObjectName(QStringLiteral("actionInterval"));
        actionThreshold = new QAction(MainWindow);
        actionThreshold->setObjectName(QStringLiteral("actionThreshold"));
        actionQVGA = new QAction(MainWindow);
        actionQVGA->setObjectName(QStringLiteral("actionQVGA"));
        actionQVGA->setCheckable(true);
        actionHVGA = new QAction(MainWindow);
        actionHVGA->setObjectName(QStringLiteral("actionHVGA"));
        actionHVGA->setCheckable(true);
        actionVGA = new QAction(MainWindow);
        actionVGA->setObjectName(QStringLiteral("actionVGA"));
        actionVGA->setCheckable(true);
        actionVGA->setChecked(true);
        actionViewReconstructedImage = new QAction(MainWindow);
        actionViewReconstructedImage->setObjectName(QStringLiteral("actionViewReconstructedImage"));
        actionViewReconstructedImage->setCheckable(true);
        actionViewReconstructedImage->setChecked(false);
        actionViewRefAndDeltaImage = new QAction(MainWindow);
        actionViewRefAndDeltaImage->setObjectName(QStringLiteral("actionViewRefAndDeltaImage"));
        actionViewRefAndDeltaImage->setCheckable(true);
        actionViewRefAndDeltaImage->setChecked(false);
        actionViewAllImages = new QAction(MainWindow);
        actionViewAllImages->setObjectName(QStringLiteral("actionViewAllImages"));
        actionViewAllImages->setCheckable(true);
        actionViewAllImages->setChecked(true);
        actionSaveReconstructedImage = new QAction(MainWindow);
        actionSaveReconstructedImage->setObjectName(QStringLiteral("actionSaveReconstructedImage"));
        actionSaveReconstructedImage->setCheckable(false);
        actionSaveReconstructedImage->setChecked(false);
        actionSaveReferenceImage = new QAction(MainWindow);
        actionSaveReferenceImage->setObjectName(QStringLiteral("actionSaveReferenceImage"));
        actionSaveReferenceImage->setCheckable(false);
        actionSaveDeltaImage = new QAction(MainWindow);
        actionSaveDeltaImage->setObjectName(QStringLiteral("actionSaveDeltaImage"));
        actionSaveDeltaImage->setCheckable(false);
        actionExport = new QAction(MainWindow);
        actionExport->setObjectName(QStringLiteral("actionExport"));
        actionClear = new QAction(MainWindow);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, -1, 9);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 884, 295));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 1, 0, 1, 1);

        deltaImageLabel = new QLabel(scrollAreaWidgetContents);
        deltaImageLabel->setObjectName(QStringLiteral("deltaImageLabel"));
        deltaImageLabel->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(deltaImageLabel, 1, 3, 1, 2, Qt::AlignLeft|Qt::AlignTop);

        reconstructedImageTitle = new QLabel(scrollAreaWidgetContents);
        reconstructedImageTitle->setObjectName(QStringLiteral("reconstructedImageTitle"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(reconstructedImageTitle->sizePolicy().hasHeightForWidth());
        reconstructedImageTitle->setSizePolicy(sizePolicy);
        reconstructedImageTitle->setMinimumSize(QSize(0, 30));
        reconstructedImageTitle->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 142);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
""));
        reconstructedImageTitle->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(reconstructedImageTitle, 3, 2, 1, 2, Qt::AlignHCenter);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 4, 4, 1, 2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 4, 0, 1, 2);

        referenceImageLabel = new QLabel(scrollAreaWidgetContents);
        referenceImageLabel->setObjectName(QStringLiteral("referenceImageLabel"));
        referenceImageLabel->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(referenceImageLabel, 1, 1, 1, 2, Qt::AlignLeft|Qt::AlignTop);

        deltaImageTitle = new QLabel(scrollAreaWidgetContents);
        deltaImageTitle->setObjectName(QStringLiteral("deltaImageTitle"));
        sizePolicy.setHeightForWidth(deltaImageTitle->sizePolicy().hasHeightForWidth());
        deltaImageTitle->setSizePolicy(sizePolicy);
        deltaImageTitle->setMinimumSize(QSize(0, 30));
        deltaImageTitle->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 142);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
""));
        deltaImageTitle->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(deltaImageTitle, 0, 3, 1, 2);

        reconstructedImageLabel = new QLabel(scrollAreaWidgetContents);
        reconstructedImageLabel->setObjectName(QStringLiteral("reconstructedImageLabel"));
        reconstructedImageLabel->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(reconstructedImageLabel, 4, 2, 1, 2, Qt::AlignHCenter|Qt::AlignTop);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_5, 1, 5, 1, 1);

        referenceImageTitle = new QLabel(scrollAreaWidgetContents);
        referenceImageTitle->setObjectName(QStringLiteral("referenceImageTitle"));
        sizePolicy.setHeightForWidth(referenceImageTitle->sizePolicy().hasHeightForWidth());
        referenceImageTitle->setSizePolicy(sizePolicy);
        referenceImageTitle->setMinimumSize(QSize(0, 30));
        referenceImageTitle->setStyleSheet(QLatin1String("background-color: rgb(0, 0, 142);\n"
"font: 75 8pt \"MS Shell Dlg 2\";\n"
""));
        referenceImageTitle->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(referenceImageTitle, 0, 1, 1, 2);

        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 1, 0, 1, 1);

        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));

        gridLayout->addWidget(tableWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 904, 21));
        menuCapture = new QMenu(menuBar);
        menuCapture->setObjectName(QStringLiteral("menuCapture"));
        menuConfigure = new QMenu(menuBar);
        menuConfigure->setObjectName(QStringLiteral("menuConfigure"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuResolution = new QMenu(menuView);
        menuResolution->setObjectName(QStringLiteral("menuResolution"));
        menuImage = new QMenu(menuView);
        menuImage->setObjectName(QStringLiteral("menuImage"));
        menuAlerts = new QMenu(menuBar);
        menuAlerts->setObjectName(QStringLiteral("menuAlerts"));
        menuSave = new QMenu(menuBar);
        menuSave->setObjectName(QStringLiteral("menuSave"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuCapture->menuAction());
        menuBar->addAction(menuConfigure->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuAlerts->menuAction());
        menuBar->addAction(menuSave->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuCapture->addAction(actionCaptureReferenceImage);
        menuCapture->addAction(actionCaptureDeltaImage);
        menuConfigure->addAction(actionInterval);
        menuConfigure->addAction(actionThreshold);
        menuView->addAction(menuResolution->menuAction());
        menuView->addAction(menuImage->menuAction());
        menuResolution->addAction(actionQVGA);
        menuResolution->addAction(actionHVGA);
        menuResolution->addAction(actionVGA);
        menuImage->addAction(actionViewReconstructedImage);
        menuImage->addAction(actionViewRefAndDeltaImage);
        menuImage->addAction(actionViewAllImages);
        menuAlerts->addAction(actionExport);
        menuAlerts->addAction(actionClear);
        menuSave->addAction(actionSaveReconstructedImage);
        menuSave->addAction(actionSaveReferenceImage);
        menuSave->addAction(actionSaveDeltaImage);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Still Image Wi-Hart Monitor", 0));
        actionCaptureReferenceImage->setText(QApplication::translate("MainWindow", "Reference Image", 0));
        actionCaptureDeltaImage->setText(QApplication::translate("MainWindow", "Delta Image", 0));
        actionInterval->setText(QApplication::translate("MainWindow", "Interval", 0));
        actionThreshold->setText(QApplication::translate("MainWindow", "Threshold", 0));
        actionQVGA->setText(QApplication::translate("MainWindow", "QVGA 320 x 240", 0));
        actionHVGA->setText(QApplication::translate("MainWindow", "HVGA 480 x 320", 0));
        actionVGA->setText(QApplication::translate("MainWindow", "VGA    640 x 480", 0));
        actionViewReconstructedImage->setText(QApplication::translate("MainWindow", "Reconstructed Image", 0));
        actionViewRefAndDeltaImage->setText(QApplication::translate("MainWindow", "Reference Image and Delta Image", 0));
        actionViewAllImages->setText(QApplication::translate("MainWindow", "All 3 Images", 0));
        actionSaveReconstructedImage->setText(QApplication::translate("MainWindow", "Reconstructed Image", 0));
        actionSaveReferenceImage->setText(QApplication::translate("MainWindow", "Reference Image", 0));
        actionSaveDeltaImage->setText(QApplication::translate("MainWindow", "Delta Image", 0));
        actionExport->setText(QApplication::translate("MainWindow", "Export", 0));
        actionClear->setText(QApplication::translate("MainWindow", "Clear", 0));
        deltaImageLabel->setText(QString());
        reconstructedImageTitle->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#ffffff;\">Reconstructed Image</span></p></body></html>", 0));
        referenceImageLabel->setText(QString());
        deltaImageTitle->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#ffffff;\">Delta Image</span></p></body></html>", 0));
        reconstructedImageLabel->setText(QString());
        referenceImageTitle->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" color:#ffffff;\">Reference Image</span></p></body></html>", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Timestamp", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Message", 0));
        menuCapture->setTitle(QApplication::translate("MainWindow", "CAPTURE", 0));
        menuConfigure->setTitle(QApplication::translate("MainWindow", "CONFIGURE", 0));
        menuView->setTitle(QApplication::translate("MainWindow", "VIEW", 0));
        menuResolution->setTitle(QApplication::translate("MainWindow", "Resolution", 0));
        menuImage->setTitle(QApplication::translate("MainWindow", "Image", 0));
        menuAlerts->setTitle(QApplication::translate("MainWindow", "ALERTS", 0));
        menuSave->setTitle(QApplication::translate("MainWindow", "SAVE", 0));
        menuHelp->setTitle(QApplication::translate("MainWindow", "HELP", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
