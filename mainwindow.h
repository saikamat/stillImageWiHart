/*******************************************************************************
* Filename              :mainwindow.h
* Description of file	:This header file consists of all the function definitions
*                        required.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Novenber 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once

#include "cSvmClassifier.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QTimer *pTimer;
    void rotateImage(QString strPath, QLabel *pLabel, tS32 iWidth, tS32 iHeight);
    void rotateAndSaveImage(const tU8 *pCPath);
    void displayImages();
    void response();
    void rtcResponse();
    void hideUncheckedImage(QLabel *pLabel, QLabel *pLabelTitle);
    void showCheckedImage(QLabel *pLabel, QLabel *pLabelTitle);
    tS32 caseId();
    void caseSelectiveDisplay(tS32 iCaseNum);
    void displayInQVGA();
    void displayInHVGA();
    void displayInVGA();
    void saveReconstructedTimeStamp();
    QString rectifyTimeFormat(QString strTsCurr);
    void registerAlertToLog(QString strTsCurr, QString strAlertMessage);
    QString extractTimeStamp(tS32 iTextFileId);
    QString extractLightingConditionStatus();
    void saveImageToBackUp(tS32 iImageId, QString strTsCurr);
    void setLabelDimensions();
    void insertRow(tS32 iRowCount);
    void populateLogTable(QString strTsCurr, QString strAlertMessage, tS32 iRowCount);
    void resumeImageView();
    void classify();
    void saveImageAs(QString strPath, IplImage *pInputImg,
                     QByteArray pathByteArray, const tU8 *pPath);
    void objectDetectedAlert(tS32 iObjectId);

public slots:
    void timerSlot();
    void alertSlot(QString strMessage);
    void timeStampSlot(QString strMessage);
    void rtcSlot();

private:
    Ui::MainWindow *ui;
    void displayStatus();
    void createActions();
    void createViewActions();
    void createAdjustResolutionActions();
    void createSaveActions();
    void createAlertsActions();
    QSignalMapper *pSignalMapperResolutions, *pSignalMapperView, *pSignalMapperSave,
    *pSignalMapperExport;
    AlertsThread *pAlertsThread;
    TimeStampThread *pTSThread;
    cSvmClassifier pLoad;
    RTCRequestThread *pRTCThread;   // pointer obj for rtc monitoring thread


private slots:
    void refreshReferenceImage();
    void refreshDeltaImage();
    void threshold();
    void interval();
    void setResolution(int iResolutionId);
    void displaySelectedImages(int iViewId);
    void save(int iSaveId);
    void help();
    void displayLogTable();
    void restOfTable();
    void exportContents();
    void clearContents();
    void clearContentsWhenMaxedOut();
};
void reconstruct(IplImage *pReferenceImage, IplImage *pDeltaImage);

#endif // MAINWINDOW_H
