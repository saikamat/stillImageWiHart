#-------------------------------------------------
#
# Project created by QtCreator 2014-06-12T17:16:08
#
#-------------------------------------------------
QMAKE_LFLAGS += /INCREMENTAL:NO
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = stillImageWiHartPCApplication
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    alertsthread.cpp \
    intervaldialog.cpp \
    thresholddialog.cpp \
    timestampthread.cpp \
    cSvmClassifier.cpp \
    rtcrequestthread.cpp

HEADERS  += mainwindow.h \
    alertsthread.h \
    intervaldialog.h \
    thresholddialog.h \
    timestampthread.h \
    definitions.h \
    cSvmClassifier.h \
    rtcrequestthread.h

FORMS    += mainwindow.ui \
    intervaldialog.ui \
    thresholddialog.ui

RESOURCES += \
    ImageResource.qrc

INCLUDEPATH +=C:\opencv\build\include
INCLUDEPATH +=C:\opencv\build\include\opencv2

LIBS += C:\opencv\build\x86\vc10\lib\*.lib
