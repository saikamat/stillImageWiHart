/*******************************************************************************
* Filename              :intervaldialog.h
* Description of file	:This header file consists of private slot which handles
*                        interval modifications.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef INTERVALDIALOG_H
#define INTERVALDIALOG_H
#pragma once
#include "definitions.h"

namespace Ui {
class IntervalDialog;
}

class IntervalDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IntervalDialog(QWidget *parent = 0);
    ~IntervalDialog();

private slots:
    void on_intervalOKPushButton_clicked();

private:
    Ui::IntervalDialog *ui;
};

#endif // INTERVALDIALOG_H
