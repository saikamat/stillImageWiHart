/*******************************************************************************
* Filename              :cSvmClassifier.h
* Description of file	:This header file consists of CvSVM class objects, function
*                        declarations and variables for multi class classification.
* Author                :Sai Kamat
* Creation Date         :Aug 06, 2014
* Last Modified Date    :November 12, 2014
* Version               :1.1
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/

#ifndef CSVMCLASSIFIER_H
#define CSVMCLASSIFIER_H
#pragma once
#include "definitions.h"
#define KERNAL_LENGTH (tS32) 05

extern tBoolean bIRLedFlag;

class cSvmClassifier
{

public:

    CvSVM oSvmPeople;
    CvSVM oSvmVehicle;
    Mat mDeltaImage;
    void recognition();
    tS32 nGetVehicleCount();
    tS32 nGetPeopleCount();
    void preProcessing();
    /*******************************************************************************
     * Function      :cSvmClassifier constructor
     * Description	:This constructor consists of CvSVM class objects, function
     *                declarations and variables for multi class classification.
     * Assumptions	:NA
     * Parameters	:NA
     * Returns       :NA
     *******************************************************************************/

    cSvmClassifier() : m_nPeopleCount(0), m_nVehicleCount(0) // constructor
    {
    }

    ~cSvmClassifier() // destructor function
    {
        mDeltaImage.release();
    }

private:
    tS32 m_nPeopleCount;
    tS32 m_nVehicleCount;
    tF32 svmPredict(vector<tF32> feature, char);
    vector<tF32> hogFeature(const Mat &grayImage, char);
    void classifier();
    Mat imageEnhancement(Mat Image);
    Mat imageSharpening(Mat Image);
};
#endif
