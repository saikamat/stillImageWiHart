/*******************************************************************************
* Filename              :rtcrequestthread.h
* Description of file	:This header file consists of path which stores the rtc
*                        request and response files and signals for rtc slot.
* Author                :Sai Kamat
* Creation Date         :August 28, 2014
* Last Modified Date    :Novenber 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef RTCREQUESTTHREAD_H
#define RTCREQUESTTHREAD_H
#pragma once
#include "definitions.h"

typedef int tS32;

class RTCRequestThread : public QThread
{
    Q_OBJECT
public:
    explicit RTCRequestThread(QObject *parent = 0);
    void run();
    QString strName;
    void monitorForRTCRequest();
    tS32 exec();

signals:
    void testSignal(QString strMessage);
    void rtcRequestSignal();

public slots:
};

#endif // RTCREQUESTTHREAD_H
