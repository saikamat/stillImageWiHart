/*******************************************************************************
* Filename              :main.cpp
* Description of file	:This source file is the starting point of the Qt
*                        application code.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "mainwindow.h"
#include <QApplication>

/*******************************************************************************
* Function      :main
* Description	:This function calls the mainwindow object and manisfests the
*                object as a GUI.
* Assumptions	:NA
* Parameters	:int argc, char *argv[]
* Returns       :An integer
*                0-> on success
*******************************************************************************/
tS32 main(tS32 argc, tU8 *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
