/*******************************************************************************
* FileName              :cSvmClassifier.cpp
* Description of file	:This source file contains all function definitions
*                        which are used by the Classifier Module.
* Author                :Sai Kamat
* Creation Date         :Aug 06, 2014
* Last Modified Date    :Novenber 12, 2014
* Version               :1.3
*******************************************************************************/
#include "cSvmClassifier.h"

/*******************************************************************************
* Function     :recognition
* Description  :This function takes two checks for correct delta and
*               reconstructed Images .
* Assumptions  :NA
* Parameters   :NA
* Returns      :void
*******************************************************************************/

void cSvmClassifier:: recognition()
{
    Mat oMatDeltaImage= imread(DELTA_IMAGE_STORAGE_PATH);
    mDeltaImage = oMatDeltaImage;
    //if (bIRLedFlag == 1)
    //{
    //    mDeltaImage=imageEnhancement(mDeltaImage);
    //    mDeltaImage=imageSharpening(mDeltaImage);
    //    mDeltaImage.convertTo(mDeltaImage, -1, 1.5, 0);
   // }
    m_nPeopleCount=0;
    m_nVehicleCount=0;
    classifier();
    oMatDeltaImage.release();
}

/*******************************************************************************
* Function     : svmPredict
* Description  : This function takes feature vector as input to predict the test image, whether Humman/Vehicle is present in the image or not

* Assumptions  : NA
* Parameters   : vector<float>
* Returns      : Float

*******************************************************************************/

tF32 cSvmClassifier :: svmPredict(vector<tF32> feature, char option)
{
    Mat featureVector(feature);
    if(option == 'p')  // for recognising Human
    {
        tF32 result = oSvmPeople.predict(featureVector);
        return result;
    }

    else if(option == 'v') // for recognising Vehicle
    {
        tF32 result = oSvmVehicle.predict(featureVector);
        return result;
    }
}

/*******************************************************************************
* Function     : hogFeature
* Description  : This function takes Mat Image as input, used to generate the features of the People and Vehicle in the Image

* Assumptions  : NA
* Parameters   : Mat
* Returns      : vector of float

*******************************************************************************/

vector<tF32> cSvmClassifier:: hogFeature(const Mat &grayImage,char temp)
{
    Mat grayTemp;
    HOGDescriptor oExtractfeaturesppl;
    vector<tF32> descriptorsValues;
    vector<Point> locations;

    if (temp == 'p') // Extract feature for Human
    {
        resize(grayImage, grayTemp, Size(HOG_IMAGE_WIDTH,HOG_IMAGE_HEIGHT));
        oExtractfeaturesppl.compute(grayTemp, descriptorsValues, Size(0,0), Size(0,0), locations);
        return descriptorsValues;
    }
    else if(temp == 'v') // Extract feature for Human
    {
        resize(grayImage, grayTemp, Size(HOG_IMAGE_HEIGHT,HOG_IMAGE_WIDTH));
        oExtractfeaturesppl.winSize=Size(HOG_IMAGE_HEIGHT,HOG_IMAGE_WIDTH);
        oExtractfeaturesppl.compute(grayTemp, descriptorsValues, Size(0,0), Size(0,0), locations);
        return descriptorsValues;
    }
}

/*******************************************************************************
* Function     :classifier
* Description  :This function does a preprocessing on the Delta and Reconstructed
*               images, generated ROI and the picks the best ROI in the Test
*               image where the object is likely to be present, and sends it
*               for Classification.
* Assumptions  : NA
* Parameters   : NA
* Returns      : void
*******************************************************************************/
void cSvmClassifier:: classifier()
{
    Mat oMatReconstructedImage = imread(RECONSTRCUTED_IMAGE_STORAGE_PATH);

    oMatReconstructedImage=imageEnhancement(oMatReconstructedImage);
    oMatReconstructedImage=imageSharpening(oMatReconstructedImage);
    oMatReconstructedImage.convertTo(oMatReconstructedImage, -1, 1.5, 0);

    Mat binaryMat,GrayImage;
    vector<vector<Point>> contours;        // Vector for storing contour used to find the contour for applying the area filter
    vector<vector<Point>> contoursRepeat; //  Vector for storing contour used for filling the holes
    vector<Vec4i> hierarchy;
    Rect bounding_Rect;

    // Converting the Delta Image to a Binary Image
    cvtColor(mDeltaImage,GrayImage,CV_BGR2GRAY);
    threshold(GrayImage, binaryMat, THRESHOLD_PARAMETER_VALUE, WHITE_PIXEL_VALUE,THRESH_BINARY);

    findContours( binaryMat, contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE ); // Find the contours in the image

    for( tS32 i = 0; i< contours.size(); i++ ) // iterate through each contour.
    {
        //  Find the area of contour

            drawContours(binaryMat, contours, i,Scalar(255), CV_FILLED);

    }

    findContours( binaryMat, contoursRepeat, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE ); // Find the contours after area threshold and removing Holes

    for( tS32 j = 0; j < contoursRepeat.size(); j++ )
    {
        Mat croppedImage;
        double a=contourArea( contoursRepeat[j],false);
        if(a > CONTOUR_COEFFICIENT_2)
        {
            bounding_Rect=boundingRect(contoursRepeat[j]); // Find the bounding rectangle for biggest contour
            croppedImage = oMatReconstructedImage(bounding_Rect);

            cvtColor(croppedImage,croppedImage,CV_BGR2GRAY);

            vector<tF32> featurePeople = hogFeature(croppedImage,'p');
            vector<tF32> featureVehicle = hogFeature(croppedImage,'v');
            tF32 fResult_Vehicle = svmPredict(featureVehicle, 'c');
            tF32 fResult_People = svmPredict(featurePeople, 'p');

            if(fResult_People == 1)
            {
                m_nPeopleCount++;
            }

            if(fResult_Vehicle == 1)
            {
                m_nVehicleCount++;
            }
        }
    }
    oMatReconstructedImage.release();
}
/*******************************************************************************
* Function     :nGetVehicleCount
* Description  :This function gets the Count of the Vehicle that is detected
*               in the classifier fucntion
* Assumptions  :NA
* Parameters   :NA
* Returns      :an integer
*******************************************************************************/
tS32 cSvmClassifier::nGetVehicleCount()
{
    return m_nVehicleCount;
}

/*******************************************************************************
* Function     :nGetPeopleCount
* Description  :This function gets the Count of the People that is detected
*               in the classifier fucntion.
* Assumptions  :NA
* Parameters   :NA
* Returns      :an integer
*******************************************************************************/
tS32 cSvmClassifier::nGetPeopleCount()
{
    return m_nPeopleCount;
}

/*******************************************************************************
* Function     :imageEnhancement
* Description  :This function enhance the IR illuminated images.
* Assumptions  :NA
* Parameters   :Mat image
* Returns      :Mat image
*******************************************************************************/
Mat cSvmClassifier::imageEnhancement(Mat oMatMirImage)
{
    Mat oMatFilteredImage;
    bilateralFilter ( oMatMirImage, oMatFilteredImage, KERNAL_LENGTH, 2*KERNAL_LENGTH, (KERNAL_LENGTH/2) );
    return oMatFilteredImage;
}

/*******************************************************************************
* Function     :imageSharpening
* Description  :This function sharpen the enhanced IR images.
* Assumptions  :NA
* Parameters   :Mat image
* Returns      :Mat image
*******************************************************************************/
Mat cSvmClassifier::imageSharpening(Mat oMatMirImage)
{
    Mat oMatSharpenedImage;
    cv::GaussianBlur(oMatMirImage, oMatSharpenedImage, cv::Size(0, 0), 2);
    cv::addWeighted(oMatMirImage, 1.5, oMatSharpenedImage, -0.5, 0, oMatSharpenedImage);
    return oMatSharpenedImage;
}
