/*******************************************************************************
* FileName              :intervaldialog.cpp
* Description of file	:This source file accepts user strResponse to change the
*                        durations of capturing the reference and delta images
*                        and writes the values to a text file.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "intervaldialog.h"
#include "ui_intervaldialog.h"

/*******************************************************************************
* Function      :IntervalDialog
* Description   :This is a simple constructor, used to setup the UI window.
* Assumptions	:NA
* Parameters	:QWidget *parent
* Returns       :NA
*******************************************************************************/
IntervalDialog::IntervalDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IntervalDialog)
{
    ui->setupUi(this);
}

/*******************************************************************************
* Function      :~IntervalDialog
* Description   :This is a simple destructor.
* Assumptions	:NA
* Parameters	:NA
* Returns       :NA
*******************************************************************************/
IntervalDialog::~IntervalDialog()
{
    delete ui;
}

/*******************************************************************************
* Function      :on_intervalOKPushButton_clicked
* Description   :This function is executed when the user wishes to change the
*                intervals for capturing reference and delta images. The values
*                are loaded to a text file, which is subsequently accepted by
*                device.
* Assumptions	:NA
* Parameters	:None
* Returns       :NA
*******************************************************************************/
void IntervalDialog::on_intervalOKPushButton_clicked()
{
    QFile::remove(CMD_HANDLER_STORAGE_PATH);
    QString strFileName = CMD_HANDLER_STORAGE_PATH;
    QFile file(strFileName);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        if((ui->spinBoxReference->text().toInt()) >
                ((ui->spinBoxDelta->text().toInt()) + INTERVAL_COMPARE_CONSTANT))
        {
            stream << "updateInterval 3 "<<ui->spinBoxDelta->text()<<" "
                   <<ui->spinBoxReference->text();
            file.close();
            QString strResponse = USB_RESPONSE_PATH;
            QFile resp(strResponse);
            resp.open(QIODevice::WriteOnly);
            resp.close();
            QFile resp1(strResponse);           // check for response
            tU8 cBuf[CHARACTER_BUFFER_VALUE];
            tU8 cBuf1[] = USB_RESPONSE_OK;
            tU8 cBuf2[] = USB_RESPONSE_INVALID;

            resp1.open(QIODevice::ReadOnly);
            while(resp1.size() == EMPTY_FILE_SIZE_VALUE)
            {
                 cout<<"on_intervalOKPushButton_clicked" <<endl;
          //      Sleep(RESPONSE_SLEEP_VALUE);
            }
            qint64 lineLength = resp1.readLine(cBuf, sizeof(cBuf));
            resp1.close();
            if(strcmp(cBuf, cBuf1) == 0)
            {
                QFile::remove(CMD_HANDLER_STORAGE_PATH);
                QFile::remove(USB_RESPONSE_PATH);
            }
            if(strcmp(cBuf, cBuf2) == 0)
            {
                QFile::remove(CMD_HANDLER_STORAGE_PATH);
                QFile::remove(USB_RESPONSE_PATH);
                QMessageBox::critical(this,tr("ERROR"),
                                      tr("Command not Recieved.\nPlease try sending "
                                         "it again.\n"));
            }
        }
        else
        {
            QMessageBox::critical(this,
                                  tr("INTERVAL CONFIGURATION ERROR"),
                                  tr("Reference image acquisition interval should be"
                                     "more than Delta Image acquisition interval."
                                     "\nPlease re-configure.\n"));
        }
        file.close();
    }
    this->close();
}
