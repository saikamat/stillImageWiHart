/*******************************************************************************
* Filename              :mainwindow.cpp
* Description of file	:This source file contains all function definitions
*                        which are used by the GUI, as well as image processing
*                        module, e.g. the reconstruction function.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Novenmer 12, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "thresholddialog.h"
#include "intervaldialog.h"

#include <QSplashScreen>
#include "Windows.h"

tBoolean bIRLedFlag;
tU8 cCommandHandle[COMMAND_HANDLE_BUFFER_VALUE];
tS32 iRowIndex = 0;
QString strTsCurr = "";
QString strAlert1 = "Large change in illumination. Please re-capture reference image";
QString strAlert2 = "The camera position has been moved or an object is obscuring"
                    "its view. Please check the device";
QString strAlert3 = "Interval Configuration Change failed";
QString strAlert4 = "Threshold Configuration Change failed";
QString strAlert5 = "RTC Set failed";
QString strAlert6 = "Ambient Light sensor is not working. Please check the device";
QString strAlert7 = "The current lighting condition is changed and is Day Light";
QString strAlert8 = "The current lighting condition is changed and is IR Light";

/*******************************************************************************
* Function      :MainWindow constructor
* Description	:This function calls the mainwindow object and manisfests the
*                object as a GUI.
* Assumptions	:NA
* Parameters	:QWidget *parent
* Returns       :NA
*******************************************************************************/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QSplashScreen splash;
    QPixmap pixmap(":/SplashScreen.png");
    splash.setPixmap(pixmap);
    splash.setWindowOpacity(0.5);
    splash.show();
    Sleep(UINT(1000));

    qApp->processEvents();
    splash.showMessage("********Loading Application**********", Qt::AlignLeft, Qt::black);
    Sleep(UINT(1000));

    //pLoad = new cSvmClassifier();
    pLoad.oSvmPeople.load(PEOPLE_CLASSIFIERS_PATH);
    pLoad.oSvmVehicle.load(CAR_CLASSIFIERS_PATH);
    ui->tableWidget->hide();
    ui->scrollArea->show();

    splash.setWindowOpacity(0.4);
    splash.showMessage("********Establishing connections**********", Qt::AlignHCenter, Qt::black);
    Sleep(UINT(1000));

    resumeImageView();
    createActions();
    insertRow(MAX_ROW_COUNT_ALERTS_LOG_TABLE);
    pAlertsThread = new AlertsThread(this);
    pTSThread = new TimeStampThread(this);
    pRTCThread = new RTCRequestThread(this);    // create obj and allocate memory
    pTimer = new QTimer(this);
    pAlertsThread->name = "mThread1";
    pTSThread->name = "pTSThread";
    //check here why name's not coming.
    connect(pAlertsThread, SIGNAL(alertSignal(QString)), this,
            SLOT(alertSlot(QString)), Qt::DirectConnection);
    connect(pTSThread, SIGNAL(timeStampSignal(QString)), this,
            SLOT(timeStampSlot(QString)), Qt::DirectConnection);
    connect(pRTCThread, SIGNAL(rtcRequestSignal()), this,
            SLOT(rtcSlot()), Qt::DirectConnection);
    connect(pTimer, SIGNAL(timeout()), this, SLOT(timerSlot()));
    pAlertsThread->start();
    pTSThread->start();
    pRTCThread->start();                        // start the thread
    pTimer->start(TIMER_VALUE);
    QString appPath = SERVER_PATH;

    splash.setWindowOpacity(0.7);
    splash.showMessage("********Starting Application**********", Qt::AlignRight, Qt::black);
    Sleep(UINT(1000));
    splash.setWindowOpacity(0.75);
    QDesktopServices::openUrl(QUrl::fromLocalFile(appPath));
    splash.close();
}

/*******************************************************************************
* Function      :~MainWindow
* Description   :This is a simple destructor. It deletes all pointers.
* Assumptions	:NA
* Parameters	:NA
* Returns       :NA
*******************************************************************************/
MainWindow::~MainWindow()
{
    delete ui;
    //delete pLoad;
    pAlertsThread->terminate();
    while(!pAlertsThread->isFinished())             // deleting the alerts thread
    {

    }
    if(!pAlertsThread->isRunning())
        qDebug()<<"alerts thread has stopped";
    delete pAlertsThread;
    pTSThread->terminate();
    while(!pTSThread->isFinished())                 // deleting the time-stamp thread
    {

    }
    if(!pTSThread->isRunning())
        qDebug()<<"timestamp thread has stopped";
    delete pTSThread;
    pRTCThread->terminate();
    while(!pRTCThread->isFinished())                // deleting the RTC thread
    {

    }
    if(!pRTCThread->isRunning())
        qDebug()<<"RTC thread has stopped";
    delete pRTCThread;
    delete pTimer;
    delete pSignalMapperResolutions;
    delete pSignalMapperSave;
    delete pSignalMapperView;
}

/*******************************************************************************
* Function      :createActions
* Description	:It creates basic action framework for sub menu and menubar
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::createActions()
{
    displayStatus();
    connect(ui->actionCaptureReferenceImage, SIGNAL(triggered()), this,
            SLOT(refreshReferenceImage()));
    connect(ui->actionCaptureDeltaImage, SIGNAL(triggered()), this,
            SLOT(refreshDeltaImage()));
    connect(ui->actionThreshold, SIGNAL(triggered()), this, SLOT(threshold()));
    connect(ui->actionInterval, SIGNAL(triggered()), this, SLOT(interval()));
    createAdjustResolutionActions();
    createViewActions();
    createSaveActions();
    createAlertsActions();
    connect(ui->menuHelp, SIGNAL(aboutToShow()), this, SLOT(help()));
}

/*******************************************************************************
* Function      :displayStatus
* Description	:It notifies the user, the effect of clicking on a sub menu
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayStatus()
{
    ui->actionCaptureReferenceImage->setStatusTip(tr("Refresh Reference image"));
    ui->actionCaptureDeltaImage->setStatusTip(tr("Refresh Delta image"));
    ui->actionInterval->setStatusTip(tr("Set the interval for capturing delta "
                                        "& reference images"));
    ui->actionThreshold->setStatusTip(tr("Set the area and intensity thresholds"));
    ui->actionQVGA->setStatusTip(tr("View images in QVGA 320 x 240 resolution"));
    ui->actionHVGA->setStatusTip(tr("View images in HVGA 480 x 320 resolution"));
    ui->actionVGA->setStatusTip(tr("View images in VGA 640 x 480 resolution"));
    ui->actionViewAllImages->setStatusTip(tr("Display all images"));
    ui->actionViewRefAndDeltaImage->setStatusTip(tr("Display Reference Image "
                                                    "& Delta Image"));
    ui->actionViewReconstructedImage->setStatusTip(tr("Display Reconstructed Image"));
    ui->actionSaveReferenceImage->setStatusTip(tr("Save Reference Image"));
    ui->actionSaveDeltaImage->setStatusTip(tr("Save Delta Image"));
    ui->actionSaveReconstructedImage->setStatusTip(tr("Save Reconstructed Image"));
    ui->actionExport->setStatusTip(tr("Save Log messages to a CSV file"));
    ui->actionClear->setStatusTip(tr("Clear Log messages"));
}

/*******************************************************************************
* Function      :createAdjustResolutionActions
* Description	:It connects action to function to adjust resolution sizes.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::createAdjustResolutionActions()
{
    pSignalMapperResolutions = new QSignalMapper(this);
    connect(pSignalMapperResolutions, SIGNAL(mapped(int)), SLOT(setResolution(int)));
    connect(ui->actionQVGA, SIGNAL(triggered()), pSignalMapperResolutions, SLOT(map()));
    connect(ui->actionHVGA, SIGNAL(triggered()), pSignalMapperResolutions, SLOT(map()));
    connect(ui->actionVGA, SIGNAL(triggered()), pSignalMapperResolutions, SLOT(map()));

    pSignalMapperResolutions->setMapping(ui->actionQVGA, 0);
    pSignalMapperResolutions->setMapping(ui->actionHVGA, 1);
    pSignalMapperResolutions->setMapping(ui->actionVGA, 2);
}

/*******************************************************************************
* Function      :createViewActions
* Description	:It creates & connects sub menu action with function for adjusting
*                view.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::createViewActions()
{
    pSignalMapperView = new QSignalMapper(this);
    connect(pSignalMapperView, SIGNAL(mapped(int)), SLOT(displaySelectedImages(int)));
    connect(ui->actionViewAllImages, SIGNAL(triggered()), pSignalMapperView,
            SLOT(map()));
    connect(ui->actionViewRefAndDeltaImage, SIGNAL(triggered()), pSignalMapperView,
            SLOT(map()));
    connect(ui->actionViewReconstructedImage, SIGNAL(triggered()), pSignalMapperView,
            SLOT(map()));
    pSignalMapperView->setMapping(ui->actionViewAllImages, 0);
    pSignalMapperView->setMapping(ui->actionViewRefAndDeltaImage, 1);
    pSignalMapperView->setMapping(ui->actionViewReconstructedImage, 2);
}

/*******************************************************************************
* Function      :createSaveActions
* Description	:It creates & connects sub menu actions with function to save images.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::createSaveActions()
{
    pSignalMapperSave = new QSignalMapper(this);
    connect(pSignalMapperSave, SIGNAL(mapped(int)), SLOT(save(int)));
    connect(ui->actionSaveReferenceImage, SIGNAL(triggered()), pSignalMapperSave,
            SLOT(map()));
    connect(ui->actionSaveDeltaImage, SIGNAL(triggered()), pSignalMapperSave,
            SLOT(map()));
    connect(ui->actionSaveReconstructedImage, SIGNAL(triggered()), pSignalMapperSave,
            SLOT(map()));
    pSignalMapperSave->setMapping(ui->actionSaveReferenceImage, 0);
    pSignalMapperSave->setMapping(ui->actionSaveDeltaImage, 1);
    pSignalMapperSave->setMapping(ui->actionSaveReconstructedImage, 2);
}

/*******************************************************************************
* Function      :createAlertsActions
* Description	:It creates & connects sub menu actions with alert table.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::createAlertsActions()
{
    connect(ui->menuAlerts, SIGNAL(aboutToShow()), this, SLOT(displayLogTable()));
    connect(ui->actionExport, SIGNAL(triggered()), this, SLOT(exportContents()));
    connect(ui->actionClear, SIGNAL(triggered()), this, SLOT(clearContents()));
}

/*******************************************************************************
* Function      :resumeImageView
* Description	:This resumes the view to display the images whenever the user
*                makes the mouse pointer to hover over the submenu bar.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::resumeImageView()
{
    connect(ui->menuCapture, SIGNAL(aboutToShow()), this, SLOT(restOfTable()));
    connect(ui->menuConfigure, SIGNAL(aboutToShow()), this, SLOT(restOfTable()));
    connect(ui->menuView, SIGNAL(aboutToShow()), this, SLOT(restOfTable()));
}

/*******************************************************************************
* Function      :restOfTable
* Description	:It hides the table widget and displays the image scroll area.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::restOfTable()
{
    ui->tableWidget->hide();
    ui->scrollArea->show();
}

/*******************************************************************************
* Function      :response
* Description	:It compares for keywords “ok” and “invalid” in usbResponse.txt,
*                and creates new commandHandler.txt and usbResponse.txt, whenever
*                the search is validated.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::response()
{
    QString strResponse = USB_RESPONSE_PATH;
    QFile resp(strResponse);
    resp.open(QIODevice::WriteOnly);
    resp.close();
    QFile resp1(strResponse);
    tU8 cBuf[CHARACTER_BUFFER_VALUE]="";
    tU8 cBuf1[] = "ok";
    tU8 cBuf2[] = "invalid";
    resp1.open(QIODevice::ReadOnly);

    while(resp1.size() == EMPTY_FILE_SIZE_VALUE)
    {
        cout<<"response" <<endl;
      //  Sleep(RESPONSE_SLEEP_VALUE);
    }
    qint64 lineLength = resp1.readLine(cBuf, sizeof(cBuf));
    resp1.close();
    if(strcmp(cBuf, cBuf1) == 0)
    {
        QFile::remove(CMD_HANDLER_STORAGE_PATH);
        QFile::remove(USB_RESPONSE_PATH);
    }
    if(strcmp(cBuf, cBuf2) == 0)
    {
        QFile::remove(CMD_HANDLER_STORAGE_PATH);
        QFile::remove(USB_RESPONSE_PATH);
        QMessageBox::critical(this,tr("ERROR"),tr("Command not Recieved.\n"
                                                  "Please try sending it again.\n"));
    }
}

/*******************************************************************************
* Function      :rtcResponse
* Description	:It compares for keywords “ok” in usbRtcResponse.txt,
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::rtcResponse()
{
    QString strResponse = USB_RTC_RESPONSE_PATH;
    QFile resp(strResponse);
    resp.open(QIODevice::WriteOnly);
    resp.close();
    QFile resp1(strResponse);
    tU8 cBuf[CHARACTER_BUFFER_VALUE]="";
    tU8 cBuf1[] = "ok";
    resp1.open(QIODevice::ReadOnly);
    while(resp1.size() == EMPTY_FILE_SIZE_VALUE)
    {
      cout<<"rtcResponse" <<endl;
      //  Sleep(RESPONSE_SLEEP_VALUE);
    }
    qint64 lineLength = resp1.readLine(cBuf, sizeof(cBuf));
    resp1.close();
    if(strcmp(cBuf, cBuf1) == 0)
    {
        QFile::remove(RTC_REQUEST_MESSAGE_STORAGE_PATH);
        QFile::remove(USB_RTC_RESPONSE_PATH);
    }
}
/*******************************************************************************
* Function      :refreshReferenceImage
* Description	:It inserts commands to send a new reference image inside the
*                commandHandler.txt
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::refreshReferenceImage()
{
    QFile::remove(CMD_HANDLER_STORAGE_PATH);
    sprintf(cCommandHandle, "sendGoldenRefImg 0 0 0");
    QString strFileName = CMD_HANDLER_STORAGE_PATH;
    QFile file(strFileName);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << cCommandHandle << endl;
        file.close();
    }
    response();
}

/*******************************************************************************
* Function      :refreshDeltaImage
* Description	:It inserts commands to send a new delta image inside
*                the commandHandler.txt
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::refreshDeltaImage()
{
    QFile::remove(CMD_HANDLER_STORAGE_PATH);
    sprintf(cCommandHandle, "sendDeltaImg 2 0 0");
    QString strFileName = CMD_HANDLER_STORAGE_PATH;
    QFile file(strFileName);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << cCommandHandle << endl;
        file.close();
    }
    response();
}

/*******************************************************************************
* Function      :threshold
* Description	:It creates a new dialog where the user can modify the area
*                and intensity threshold values
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::threshold()
{
    ThresholdDialog tD;
    tD.setModal(true);
    tD.exec();
}

/*******************************************************************************
* Function      :interval
* Description	:It creates a new dialog where the user can modify the reference
*                and delta capture duration values.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::interval()
{
    IntervalDialog iD;
    iD.setModal(true);
    iD.exec();
}

/*******************************************************************************
* Function      :rotateAndSaveImage
* Description	:It rotates the image obtained from the sensor and saves it.
* Assumptions	:NA
* Parameters	:const char *pPath
* Returns       :void
*******************************************************************************/
void MainWindow::rotateAndSaveImage(const tU8 *pPath)
{
    IplImage *pImage = cvLoadImage(pPath, CV_LOAD_IMAGE_UNCHANGED);
    Mat oMatImage(pImage);
    imwrite(pPath, oMatImage);
}

/*******************************************************************************
* Function      :setResolution
* Description	:It lets the user select just one type of resolution at a time,
*                disabling the other two resolutions.
* Assumptions	:NA
* Parameters	:int resolutionId
* Returns       :void
*******************************************************************************/
void MainWindow::setResolution(int iResolutionId)
{
    switch(iResolutionId)
    {
    case 0:
        if(ui->actionQVGA->isChecked())
        {
            ui->actionHVGA->setChecked(false);
            ui->actionVGA->setChecked(false);
            displayInQVGA();
        }
        else
            QMessageBox::critical(this,tr("ERROR"),tr("Please select one resolution.\n"));
        break;
    case 1:
        if(ui->actionHVGA->isChecked())
        {
            ui->actionQVGA->setChecked(false);
            ui->actionVGA->setChecked(false);
            displayInHVGA();
        }
        else
            QMessageBox::critical(this,tr("ERROR"),tr("Please select one resolution.\n"));
        break;
    case 2:
        if(ui->actionVGA->isChecked())
        {
            ui->actionQVGA->setChecked(false);
            ui->actionHVGA->setChecked(false);
            displayInVGA();
        }
        else
            QMessageBox::critical(this,tr("ERROR"),tr("Please select one resolution.\n"));
        break;
    }
}

/*******************************************************************************
* Function      :displaySelectedImages
* Description	:This slot function calls the case for selective displaying images.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displaySelectedImages(int iViewId)
{
    switch(iViewId)
    {
    case 0:
        if(ui->actionViewAllImages->isChecked())
        {
            ui->actionViewRefAndDeltaImage->setChecked(false);
            ui->actionViewReconstructedImage->setChecked(false);
            showCheckedImage(ui->referenceImageLabel, ui->referenceImageTitle);
            showCheckedImage(ui->deltaImageLabel, ui->deltaImageTitle);
            showCheckedImage(ui->reconstructedImageLabel,
                             ui->reconstructedImageTitle);
        }
        break;
    case 1:
        if(ui->actionViewRefAndDeltaImage->isChecked())
        {
            ui->actionViewAllImages->setChecked(false);
            ui->actionViewReconstructedImage->setChecked(false);
            showCheckedImage(ui->referenceImageLabel, ui->referenceImageTitle);
            showCheckedImage(ui->deltaImageLabel, ui->deltaImageTitle);
            hideUncheckedImage(ui->reconstructedImageLabel, ui->reconstructedImageTitle);
        }
        break;
    case 2:
        if(ui->actionViewReconstructedImage->isChecked())
        {
            ui->actionViewAllImages->setChecked(false);
            ui->actionViewRefAndDeltaImage->setChecked(false);
            showCheckedImage(ui->reconstructedImageLabel, ui->reconstructedImageTitle);
            hideUncheckedImage(ui->referenceImageLabel, ui->referenceImageTitle);
            hideUncheckedImage(ui->deltaImageLabel, ui->deltaImageTitle);
        }
        break;
    }
}

/*******************************************************************************
* Function      :rotateImage
* Description	:This function rotates the image depending upon sensor
*                orientation
* Assumptions	:NA
* Parameters	:QString stringPath, QLabel *pLabel, tS32 width, tS32 height
* Returns       :void
*******************************************************************************/
void MainWindow::rotateImage(QString strPath, QLabel *pLabel, tS32 iWidth,
                             tS32 iHeight)
{
    QPixmap updatedPix1(strPath);
    pLabel->setPixmap(updatedPix1.scaled(iWidth, iHeight, Qt::KeepAspectRatio));
    QPixmap pixmap1(*(pLabel->pixmap()));
    QTransform rm;
    rm.rotate(ROTATION_ANGLE);
    pixmap1 = pixmap1.transformed(rm);
    pLabel->setPixmap(pixmap1);
}

/*******************************************************************************
* Function      :setLabelDimensions
* Description	:It sets the dimensions of the labels which contain the title.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::setLabelDimensions()
{
    ui->referenceImageTitle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    ui->deltaImageTitle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    ui->reconstructedImageTitle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    ui->referenceImageTitle->setMinimumWidth(ui->referenceImageLabel->width());
    ui->deltaImageTitle->setMinimumWidth(ui->deltaImageLabel->width());
    ui->reconstructedImageTitle->setMinimumWidth(ui->reconstructedImageLabel->width());
}

/*******************************************************************************
* Function      :displayInQVGA
* Description	:TIt displays the images in QVGA resolution
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayInQVGA()
{
    rotateImage(UPDATED_IMAGE_STORAGE_PATH, ui->referenceImageLabel,
                QVGA_WIDTH, QVGA_HEIGHT);
    rotateImage(RECONSTRCUTED_IMAGE_STORAGE_PATH, ui->reconstructedImageLabel,
                QVGA_WIDTH, QVGA_HEIGHT);
    rotateImage(DELTA_IMAGE_STORAGE_PATH, ui->deltaImageLabel,
                QVGA_WIDTH, QVGA_HEIGHT);
    setLabelDimensions();
}

/*******************************************************************************
* Function      :displayInHVGA
* Description	:It displays the images in HVGA resolution
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayInHVGA()
{
    rotateImage(UPDATED_IMAGE_STORAGE_PATH, ui->referenceImageLabel,
                HVGA_WIDTH, HVGA_HEIGHT);
    rotateImage(RECONSTRCUTED_IMAGE_STORAGE_PATH, ui->reconstructedImageLabel,
                HVGA_WIDTH, HVGA_HEIGHT);
    rotateImage(DELTA_IMAGE_STORAGE_PATH, ui->deltaImageLabel,
                HVGA_WIDTH, HVGA_HEIGHT);
    setLabelDimensions();
}

/*******************************************************************************
* Function      :displayInVGA
* Description	:It displays the images in VGA resolution
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayInVGA()
{
    rotateImage(UPDATED_IMAGE_STORAGE_PATH, ui->referenceImageLabel,
                VGA_WIDTH, VGA_HEIGHT);
    rotateImage(RECONSTRCUTED_IMAGE_STORAGE_PATH, ui->reconstructedImageLabel,
                VGA_WIDTH, VGA_HEIGHT);
    rotateImage(DELTA_IMAGE_STORAGE_PATH, ui->deltaImageLabel,
                VGA_WIDTH, VGA_HEIGHT);
    setLabelDimensions();
}


/*******************************************************************************
* Function      :hideUncheckedImage
* Description	:It hides the image label and the title label.
* Assumptions	:NA
* Parameters	:QLabel *pLabel, QLabel *pLabelTitle
* Returns       :void
*******************************************************************************/
void MainWindow::hideUncheckedImage(QLabel *pLabel, QLabel *pLabelTitle)
{
    pLabel->hide();
    pLabelTitle->hide();
}

/*******************************************************************************
* Function      :showCheckedImage
* Description	:It shows the image label and the title label.
* Assumptions	:NA
* Parameters	:QLabel *pLabel, QLabel *pLabelTitle
* Returns       :void
*******************************************************************************/
void MainWindow::showCheckedImage(QLabel *pLabel, QLabel *pLabelTitle)
{
    pLabel->show();
    pLabelTitle->show();
}

/*******************************************************************************
* Function      :displayImages
* Description	:This slot is executed every TIMER_VALUE milliseconds apart,
*                and refreshes images in real time.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayImages()
{
    if(ui->actionVGA->isChecked())
    {
        ui->actionHVGA->setChecked(false);
        ui->actionQVGA->setChecked(false);
        displayInVGA();
    }
    else if(ui->actionHVGA->isChecked())
    {
        ui->actionVGA->setChecked(false);
        ui->actionQVGA->setChecked(false);
        displayInHVGA();
    }
    else
    {
        ui->actionVGA->setChecked(false);
        ui->actionHVGA->setChecked(false);
        displayInQVGA();
    }
}

/*******************************************************************************
* Function      :timerSlot
* Description	:This slot is executed every TIMER_VALUE milliseconds apart,
*                and refreshes images in real time.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::timerSlot()
{
    displayImages();
}

void MainWindow::rtcSlot()
{
    QFile::remove(RTC_REQUEST_MESSAGE_STORAGE_PATH);
    QString strRTC = QDateTime::currentDateTime().toString(DATE_TIME_FORMAT2);
                                                      // find current time from PC
    QByteArray pathByteArray = strRTC.toLocal8Bit();  // convert string to const char
    const tU8 *pPath = pathByteArray.data();
    sprintf(cCommandHandle, pPath);
    QString strFileName = RTC_REQUEST_MESSAGE_STORAGE_PATH;
    QFile file(strFileName);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);
        stream << cCommandHandle << endl;           // put the time string into the file
        file.close();
    }
    rtcResponse();
}

/*******************************************************************************
* Function      :registerAlertToLog
* Description	:This concatenates the timestamp string with the alert message
*                and saves to a log file.
* Assumptions	:NA
* Parameters	:QString strTsCurr, QString alertMessage
* Returns       :void
*******************************************************************************/
void MainWindow::registerAlertToLog(QString strTsCurr, QString strAlertMessage)
{
    populateLogTable(strTsCurr, strAlertMessage, iRowIndex++);
    QString strLogMessage = strTsCurr + "," + strAlertMessage;
    QByteArray logByteArray = strLogMessage.toLocal8Bit();
    const tU8 *pErrorLog = logByteArray.data();
    sprintf(cCommandHandle, pErrorLog);
    QString strFileName = ALERT_MESSAGE_BACKUP_PATH;
    QFile file(strFileName);
    if (file.open(QIODevice::Append))
    {
        QTextStream stream(&file);
        stream << cCommandHandle << endl;
        file.close();
    }
}

/*******************************************************************************
* Function      :rectifyTimeFormat
* Description	:It replaces the "_" with ":" for better readability.
* Assumptions	:NA
* Parameters	:QString strTsCurr
* Returns       :A QString
*******************************************************************************/
QString MainWindow::rectifyTimeFormat(QString strTsCurr)
{
    QString strTsNew = strTsCurr.replace("_", ":");
    return(strTsNew);
}

/*******************************************************************************
* Function      :alertSlot
* Description	:This slot is executed whenever the alertSignal signal is emitted
*                by the monitorForAlerts(). It extracts timestamp, and registers
*                the timestamp with the alert.
* Assumptions	:NA
* Parameters	:QString message
* Returns       :void
*******************************************************************************/
void MainWindow::alertSlot(QString strMessage)
{

    if(strMessage == ALERT1)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        registerAlertToLog(strTsNew, strAlert1);
    }
    else if(strMessage == ALERT2)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        registerAlertToLog(strTsNew, strAlert2);
    }
    else if(strMessage == ALERT3)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        registerAlertToLog(strTsNew, strAlert3);
    }
    else if(strMessage == ALERT4)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        registerAlertToLog(strTsNew, strAlert4);
    }
    else if(strMessage == ALERT5)
    {
        QString strTsNew = "-------";
        registerAlertToLog(strTsNew, strAlert5);
    }
    else if(strMessage == ALERT6)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        registerAlertToLog(strTsNew, strAlert6);
    }
    else if(strMessage == ALERT7)
    {
        strTsCurr = extractTimeStamp(ALERT_TIMESTAMP_EXTRACTION_ID);
        QString strTsNew = rectifyTimeFormat(strTsCurr);
        QString strLightCondStatus = extractLightingConditionStatus();
        if((strLightCondStatus.compare(strLightCondStatus, LED_OFF_ID, Qt::CaseSensitive)) == 0)
        {
            bIRLedFlag = 0;
            registerAlertToLog(strTsNew, strAlert7);
        }
        else if((strLightCondStatus.compare(strLightCondStatus, LED_ON_ID, Qt::CaseSensitive)) == 0)
        {
            bIRLedFlag = 1;
            registerAlertToLog(strTsNew, strAlert8);
        }
     }
}

/*******************************************************************************
* Function      :saveReconstructedTimeStamp
* Description	:TThis saves the reconstructed image to a backup repository
*                along with the timestamp.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::saveReconstructedTimeStamp()
{
    QString strFileName = RECONSTRCUTED_IMAGE_STORAGE_PATH;
    QFileInfo info(strFileName);
    QDateTime dateTimeMod = info.lastModified();
    QString strPath = QString(RECONSTRCUTED_IMAGE_BACKUP_PATH).
            arg(dateTimeMod.toString(DATE_TIME_FORMAT));
    QByteArray pathByteArray = strPath.toLocal8Bit();   // convert strPath into
                                                        //const char*
    const tU8 *pPath = pathByteArray.data();
    IplImage *pInputImg = cvLoadImage(RECONSTRCUTED_IMAGE_STORAGE_PATH,
                                      CV_LOAD_IMAGE_UNCHANGED);
    cvSaveImage(pPath, pInputImg);
    rotateAndSaveImage(pPath);
    cvReleaseImage(&pInputImg);
}

/*******************************************************************************
* Function      :saveImageToBackUp
* Description	:It saves the images stored in current repository to a backup
*                repository with timestamp string strTsCurr.
* Assumptions	:NA
* Parameters	:int iImageId, QString strTsCurr
* Returns       :void
*******************************************************************************/
void MainWindow::saveImageToBackUp(tS32 iImageId, QString strTsCurr)
{
    if(iImageId == REFERENCE_IMAGE_ID)
    {
        QString strPath = QString(UPDATED_IMAGE_BACKUP_PATH).arg(strTsCurr);
        QByteArray pathByteArray = strPath.toLocal8Bit();     // convert strPath into
                                                              // const char*
        const tU8 *pPath = pathByteArray.data();
        IplImage *pInputImg = cvLoadImage(UPDATED_IMAGE_STORAGE_PATH,
                                          CV_LOAD_IMAGE_UNCHANGED);
        cvSaveImage(pPath,pInputImg);
        rotateAndSaveImage(pPath);
        cvReleaseImage(&pInputImg);
    }
    else if(iImageId == DELTA_IMAGE_ID)
    {
        QString strPath = QString(DELTA_IMAGE_BACKUP_PATH).arg(strTsCurr);
        QByteArray pathByteArray = strPath.toLocal8Bit();   // convert strPath into
                                                            // const char*
        const tU8 *pPath = pathByteArray.data();
        IplImage *pInputImg = cvLoadImage(DELTA_IMAGE_STORAGE_PATH,
                                          CV_LOAD_IMAGE_UNCHANGED);
        cvSaveImage(pPath,pInputImg);
        rotateAndSaveImage(pPath);
        cvReleaseImage(&pInputImg);
    }
    else if(iImageId == CURRENT_IMAGE_ID)
    {
        QString strPath = QString(CURRENT_IMAGE_BACKUP_PATH).arg(strTsCurr);
        QByteArray pathByteArray = strPath.toLocal8Bit();   // convert strPath into
                                                            // const char*
        const tU8 *pPath = pathByteArray.data();
        IplImage *pInputImg = cvLoadImage(CURRENT_IMAGE_STORAGE_PATH,
                                          CV_LOAD_IMAGE_UNCHANGED);
        cvSaveImage(pPath,pInputImg);
        rotateAndSaveImage(pPath);
        cvReleaseImage(&pInputImg);
    }
}

/*******************************************************************************
* Function      :timeStampSlot
* Description	:This slot function checks for timestamp.txt for the type of
*                image sent, and extracts the timestamp. It also calls the
*                reconstruct image function
* Assumptions	:NA
* Parameters	:QString message
* Returns       :void
*******************************************************************************/
void MainWindow::timeStampSlot(QString strMessage)
{
    IplImage *pMyPic  = cvLoadImage(UPDATED_IMAGE_STORAGE_PATH,
                                    CV_LOAD_IMAGE_UNCHANGED);
    IplImage *pMyPic1 = cvLoadImage(DELTA_IMAGE_STORAGE_PATH,
                                    CV_LOAD_IMAGE_UNCHANGED);
    if(strMessage == REFERENCE_TIMESTAMP_ID)
    {
        strTsCurr = extractTimeStamp(0);
        saveImageToBackUp(REFERENCE_IMAGE_ID, strTsCurr);
    }
    else if(strMessage == DELTA_TIMESTAMP_ID)
    {
        strTsCurr = extractTimeStamp(0);
        saveImageToBackUp(DELTA_IMAGE_ID, strTsCurr);
        reconstruct(pMyPic, pMyPic1);
        saveReconstructedTimeStamp();
        classify();
    }
    else if(strMessage == CURRENT_TIMESTAMP_ID)
    {
        strTsCurr = extractTimeStamp(0);
        saveImageToBackUp(CURRENT_IMAGE_ID, strTsCurr);
    }
    cvReleaseImage(&pMyPic);
    cvReleaseImage(&pMyPic1);
}

/*******************************************************************************
* Function      :extractTimeStamp
* Description	:Depending upon the textField value, this function extracts
*                the timestamp from the timestamp.txt or the usbAlerts.txt.
* Assumptions	:NA
* Parameters	:int iTextFileId
* Returns       :A string
*******************************************************************************/
QString MainWindow::extractTimeStamp(tS32 iTextFileId)
{
    if(iTextFileId == IMAGE_TIMESTAMP_EXTRACTION_ID)
    {
        QFile inputFile(TIMESTAMP_MESSAGE_STORAGE_PATH);
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QTextStream in(&inputFile);
            tS32 tab[9];tS32 i=0;
            while ( !in.atEnd() )
            {
                QString strLine = in.readLine();
                tab[i]=strLine.toInt();
                i++;
                if(TIMESTAMP_LINE_LOCATION_VALUE == i)
                {
                    QString strTimeStamp = strLine.mid(TIMESTAMP_EXTRACTION_POINT, -1);
                    inputFile.close();
                    return(strTimeStamp);
                }
            }
        }
    }
    else if(iTextFileId == ALERT_TIMESTAMP_EXTRACTION_ID)
    {
        QFile inputFile(ALERTS_MESSAGE_STORAGE_PATH);
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QTextStream in(&inputFile);
            tS32 tab[9];tS32 i=0;
            while ( !in.atEnd() )
            {
                QString strLine = in.readLine();
                tab[i]=strLine.toInt();
                i++;
                if(TIMESTAMP_LINE_LOCATION_VALUE == i)
                {
                    QString strTimeStamp = strLine.mid(TIMESTAMP_EXTRACTION_POINT, -1);
                    inputFile.close();
                    return(strTimeStamp);
                }
            }
        }
    }
}

/*******************************************************************************
* Function      :extractLightingConditionStatus
* Description	:This function extracts the lighting condition status from
*                usbAlerts.txt.
* Assumptions	:NA
* Parameters	:None
* Returns       :A string
*******************************************************************************/
QString MainWindow::extractLightingConditionStatus()
{
    QFile inputFile(ALERTS_MESSAGE_STORAGE_PATH);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        tS32 tab[9];tS32 i=0;
        while ( !in.atEnd() )
        {
            QString strLine = in.readLine();
            tab[i]=strLine.toInt();
            i++;
            if(LIGHTING_COND_LOCATION_VALUE == i)
            {
                inputFile.close();
                return(strLine);
            }
        }
    }
 }

/*******************************************************************************
* Function      :saveImageAs
* Description	:It creates a char array path compatible to save the image
* Assumptions	:NA
* Parameters	:QString strPath, IplImage *pInputImg,QByteArray pathByteArray,
*                const tU8 *pPath
* Returns       :void
*******************************************************************************/
void MainWindow::saveImageAs(QString strPath, IplImage *pInputImg,
                             QByteArray pathByteArray, const tU8 *pPath)
{
    pathByteArray = strPath.toLocal8Bit();     // convert strPath into const char*
    pPath = pathByteArray.data();
    cvSaveImage(pPath, pInputImg);
    rotateAndSaveImage(pPath);
    cvReleaseImage(&pInputImg);
}

/*******************************************************************************
* Function      :save
* Description	:This slot saves the image to a user selected folder on the PC
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::save(int iSaveId)
{
    QString strPath="";
    IplImage *pInputImg;
    QByteArray pathByteArray;
    const tU8 *pPath = NULL;
    switch(iSaveId)
    {
    case 0:
        strPath = QFileDialog::getSaveFileName(
                    this, tr("Save Reference Image as"),
                    "..//..//..//", "Image file(*.jpg)");
        pInputImg  = cvLoadImage(UPDATED_IMAGE_STORAGE_PATH,
                                 CV_LOAD_IMAGE_UNCHANGED);
        if("" != strPath)
        {
            saveImageAs(strPath, pInputImg, pathByteArray, pPath);
        }
        break;
    case 1:
        strPath = QFileDialog::getSaveFileName(
                    this, tr("Save Delta Image as"),
                    "..//..//..//", "Image file(*.jpg)");
        pInputImg  = cvLoadImage(DELTA_IMAGE_STORAGE_PATH,
                                 CV_LOAD_IMAGE_UNCHANGED);
        if("" != strPath)
        {
            saveImageAs(strPath, pInputImg, pathByteArray, pPath);
        }
        break;
    case 2:
        strPath = QFileDialog::getSaveFileName(
                    this, tr("Save Reconstructed Image as"),
                    "..//..//..//", "Image file(*.jpg)");
        pInputImg  = cvLoadImage(RECONSTRCUTED_IMAGE_STORAGE_PATH,
                                 CV_LOAD_IMAGE_UNCHANGED);
        if("" != strPath)
        {
            saveImageAs(strPath, pInputImg, pathByteArray, pPath);
        }
        break;
    }
}

/*******************************************************************************
* Function      :displayLogTable
* Description	:This slot is executed every TIMER_VALUE milliseconds apart,
*                and refreshes images in real time.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::displayLogTable()
{
    ui->scrollArea->hide();
    ui->tableWidget->show();
}

/*******************************************************************************
* Function      :insertRow
* Description	:This function enters a definite no of rows in the log table.
* Assumptions	:NA
* Parameters	:tS32 rowCount
* Returns       :void
*******************************************************************************/
void MainWindow::insertRow(tS32 iRowCount)
{
    for (tS32 i= 0; i < iRowCount; i++)
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
}

/*******************************************************************************
* Function      :populateLogTable
* Description	:This populates the log table with the alert and the time at which
*                they occurred.
* Assumptions	:NA
* Parameters	:QString strTsCurr, QString alertMessage, int iRowIndex
* Returns       :void
*******************************************************************************/
void MainWindow::populateLogTable(QString strTsCurr, QString strAlertMessage,
                                  tS32 iRowCount)
{
    ui->tableWidget->setItem(iRowCount, 0, new QTableWidgetItem(strTsCurr));
    ui->tableWidget->setItem(iRowCount, 1, new QTableWidgetItem(strAlertMessage));
    if((MAX_ROW_COUNT_ALERTS_LOG_TABLE - 1) == iRowCount)
    {
        static tS32 iWarningCount = 1;
        if(1 == iWarningCount)              // check for displaying warning only once.
        iWarningCount++;
    }
    else if(MAX_ROW_COUNT_ALERTS_LOG_TABLE == iRowCount)
    {
        exportContents();
        clearContentsWhenMaxedOut();
        iRowCount = 0;
        populateLogTable(strTsCurr, strAlertMessage, iRowCount++);
    }
    else if(MAX_ROW_COUNT_ALERTS_LOG_TABLE < iRowCount)
    {
        iRowCount = iRowCount - MAX_ROW_COUNT_ALERTS_LOG_TABLE;
        populateLogTable(strTsCurr, strAlertMessage, iRowCount++);
    }
}

/*******************************************************************************
* Function      :exportContents
* Description	:It saves the alerts to a separate .csv file.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::exportContents()
{
    QFile::remove(ALERT_MESSAGE_LOG_PATH);
    QFile::copy(ALERT_MESSAGE_BACKUP_PATH, ALERT_MESSAGE_LOG_PATH);
}

/*******************************************************************************
* Function      :clearContents
* Description	:It clears the contents of the log table and sets row index to 0
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::clearContents()
{
    iRowIndex = 0;
    ui->tableWidget->clearContents();
}

/*******************************************************************************
* Function      :clearContentsWhenMaxedOut
* Description	:It clears the contents of the log table.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::clearContentsWhenMaxedOut()
{
    ui->tableWidget->clearContents();
}

/*******************************************************************************
* Function      :help
* Description	:It displays the help menu.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::help()
{
    QString strDocPath = MANUAL_PATH;
    QDesktopServices::openUrl(QUrl::fromLocalFile(strDocPath));
}

/*******************************************************************************
* Function      :classify
* Description	:It classifies whether the reconstructed image has an object
*                or not. If the object exists, the user is alerted.
* Assumptions	:NA
* Parameters	:NA
* Returns       :void
*******************************************************************************/
void MainWindow::classify()
{
    pLoad.recognition();
    tS32 nVehicle_Count = pLoad.nGetVehicleCount();
    tS32 nPeople_Count = pLoad.nGetPeopleCount();
    if(nVehicle_Count>0)
    {
        objectDetectedAlert(VEHICLE_DETECTION_INDEX);
    }

    else if (nPeople_Count>0)
    {
        objectDetectedAlert(PERSON_DETECTION_INDEX);
    }
}

void MainWindow::objectDetectedAlert(tS32 iObjectId)
{
    if(VEHICLE_DETECTION_INDEX == iObjectId)
    {
        registerAlertToLog((QDateTime::currentDateTime().toString(DATE_TIME_FORMAT2)),
                           VEHICLE_DETECTED);
    }
    else if(PERSON_DETECTION_INDEX == iObjectId)
    {
        registerAlertToLog((QDateTime::currentDateTime().toString(DATE_TIME_FORMAT2)),
                           PERSON_DETECTED);
    }
}

/*******************************************************************************
* Function     :reconstruct
* Description  :The function reconstructs an image from the stored reference
*               image and delta image.
* Assumptions  :NA
* Parameters   :IplImage *pReferenceImage, IplImage *pDeltaImage
*               pReferenceImage-> It is the image which act as a reference image
*               and used used for detecting what are the things that has changed
*               in the view.
*               pDeltaImage-> The image which contains the delta(changed scene).
* Returns      :void
*******************************************************************************/
void reconstruct(IplImage *pReferenceImage, IplImage *pDeltaImage)
{
    IplImage* pDeltaY = cvCreateImage(cvSize(pReferenceImage->width,
                                             pReferenceImage->height),
                                      IPL_DEPTH_8U, 1);
    cvCvtColor(pDeltaImage, pDeltaY, CV_BGR2GRAY);
    IplImage *pBinaryDelta = cvCreateImage(cvSize(pReferenceImage->width,
                                                  pReferenceImage->height),
                                           IPL_DEPTH_8U, 1);
    cvThreshold(pDeltaY, pBinaryDelta, NOISE_THRESHOLD, WHITE, CV_THRESH_BINARY);

    IplImage *pDeltaR = cvCreateImage(cvSize(pReferenceImage->width,
                                             pReferenceImage->height),
                                      IPL_DEPTH_8U, 1);
    IplImage *pDeltaG = cvCreateImage(cvSize(pReferenceImage->width,
                                             pReferenceImage->height),
                                      IPL_DEPTH_8U, 1);
    IplImage *pDeltaB = cvCreateImage(cvSize(pReferenceImage->width,
                                             pReferenceImage->height),
                                      IPL_DEPTH_8U, 1);

    IplImage *pReferenceR = cvCreateImage(cvSize(pReferenceImage->width,
                                                 pReferenceImage->height),
                                          IPL_DEPTH_8U, 1);
    IplImage *pReferenceG = cvCreateImage(cvSize(pReferenceImage->width,
                                                 pReferenceImage->height),
                                          IPL_DEPTH_8U, 1);
    IplImage *pReferenceB = cvCreateImage(cvSize(pReferenceImage->width,
                                                 pReferenceImage->height),
                                          IPL_DEPTH_8U, 1);

    cvSplit(pReferenceImage, pReferenceR, pReferenceG, pReferenceB, NULL);
    cvSplit(pDeltaImage, pDeltaR, pDeltaG, pDeltaB, NULL);

    IplImage *pInvertDelta = cvCreateImage(cvSize(pReferenceImage->width,
                                                  pReferenceImage->height),
                                           IPL_DEPTH_8U, 1);
    cvThreshold(pDeltaY, pInvertDelta, NOISE_THRESHOLD, WHITE, CV_THRESH_BINARY_INV);

    IplImage *pReconstructImg = cvCreateImage(cvSize(pReferenceImage->width,
                                                     pReferenceImage->height),
                                              IPL_DEPTH_8U, 3);
    cvAnd(pDeltaR, pBinaryDelta, pDeltaR);
    cvAnd(pDeltaG, pBinaryDelta, pDeltaG);
    cvAnd(pDeltaB, pBinaryDelta, pDeltaB);
    cvMerge(pDeltaR, pDeltaG, pDeltaB, NULL, pDeltaImage);

    cvAnd(pReferenceR, pInvertDelta, pReferenceR);
    cvAnd(pReferenceG, pInvertDelta, pReferenceG);
    cvAnd(pReferenceB, pInvertDelta, pReferenceB);
    cvMerge(pReferenceR, pReferenceG, pReferenceB, NULL, pReconstructImg);

    cvAdd(pDeltaImage, pReconstructImg, pReconstructImg);
    cvSaveImage(RECONSTRCUTED_IMAGE_STORAGE_PATH, pReconstructImg);
    cvReleaseImage(&pDeltaY);
    cvReleaseImage(&pBinaryDelta);
    cvReleaseImage(&pInvertDelta);
    cvReleaseImage(&pReconstructImg);
    cvReleaseImage(&pDeltaR);
    cvReleaseImage(&pDeltaG);
    cvReleaseImage(&pDeltaB);
    cvReleaseImage(&pReferenceR);
    cvReleaseImage(&pReferenceG);
    cvReleaseImage(&pReferenceB);
}
