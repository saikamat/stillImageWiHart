/*******************************************************************************
* Filename              :thresholddialog.h
* Description of file	:This header file consists of private slot which handles
*                        area and intensity threshold modifications.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef THRESHOLDDIALOG_H
#define THRESHOLDDIALOG_H
#pragma once
#include "definitions.h"

namespace Ui {
class ThresholdDialog;
}

class ThresholdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ThresholdDialog(QWidget *parent = 0);
    ~ThresholdDialog();

private slots:
    void on_thresholdOKPushButton_clicked();

private:
    Ui::ThresholdDialog *ui;
};

#endif // THRESHOLDDIALOG_H
