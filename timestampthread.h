/*******************************************************************************
* Filename              :timestampthread.h
* Description of file	:This header file consists of path which stores the time
*                        stamp and signals for the timestamp.
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef TIMESTAMPTHREAD_H
#define TIMESTAMPTHREAD_H
#pragma once
#include "definitions.h"

typedef int tS32;

class TimeStampThread : public QThread
{
    Q_OBJECT
public:
    explicit TimeStampThread(QObject *parent = 0);
    void run();
    QString name;
    void monitorForTimeStamps();
    tS32 exec();

signals:
    void testSignal(QString strMessage);
    void timeStampSignal(QString strMessage);

public slots:

};

#endif // TIMESTAMPTHREAD_H
