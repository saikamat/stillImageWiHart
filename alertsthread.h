/*******************************************************************************
* Filename              :alertsthread.h
* Description of file	:This header file consists of path which stores the type
*                        of response and signals for such alerts
* Author                :Sai Kamat
* Creation Date         :May 26, 2014
* Last Modified Date    :Jun 26, 2014
* Version               :2.2
* Legal entity          :Copyright 2014
*                        All Rights Reserved.
*******************************************************************************/
#ifndef ALERTSTHREAD_H
#define ALERTSTHREAD_H
#pragma once
#include "definitions.h"

typedef int tS32;

class AlertsThread : public QThread
{
    Q_OBJECT
public:
    explicit AlertsThread(QObject *parent = 0);
    void run();
    QString name;
    void monitorForAlerts();
    tS32 exec();

signals:
    void testSignal(QString strMessage);
    void alertSignal(QString strMessage);

public slots:
};

#endif // ALERTSTHREAD_H
